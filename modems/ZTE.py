from modems.modem import Modem
from subprocess import Popen
from tempfile import mkstemp

# class for ZTE MF190 modem
class MF190(Modem):
	def __init__(self, args):
		super().__init__(args)
		self.vendor_id = "19d2"
		self.storage_product_id = "0154"
		self.modem_product_id = "0117"
		self.switch_message = "5553424312345678000000000000061e000000000000000000000000000000"
		self.switch_message2 = "5553424312345679000000000000061b000000020000000000000000000000"

	def createConfigFile(self):
		(fd, path) = mkstemp()
		file = open(path, "w")

		string = "[Dialer Defaults]\n"
		if self.pin != None:
			string += self.initLine("AT+CPIN=\"" + self.pin + "\"")
		string += self.initLine("ATZ")
		string += self.initLine("ATQ0 V1 E1 S0=0 &C1 &D2 +FCLASS=0")
		string += self.initLine("AT+CGDCONT=1,\"IP\",\"" + self.apn + "\"")
		string += "Stupid Mode = 1\n"
		string += "Modem Type = Analog Modem\n"
		string += "Phone = " + self.phone_number + "\n"
		string += "ISDN = 0\n"
		string += "Username = \"" + self.username + "\"\n"
		string += "Password = \"" + self.password + "\"\n"
		string += "Baud = " + str(self.baud_rate) + "\n"
		string += "Modem = " + self.dev + "\n"
		
		file.write(string)
		print("Config file was written to", path)
		file.close()

		return path

	def switch(self, persistent=False, persistent_action=None):
		(fd, path) = mkstemp()
		file = open(path, "w")

		string = "# ZTE MF190 (Variant) and others\n"
		string += "TargetVendor = " + self.vendor_id + "\n"
		string += "TargetProduct = " + self.modem_product_id + "\n"
		string += "MessageContent=\"" + self.switch_message + "\"\n"
		string += "MessageContent2=\"" + self.switch_message2 + "\"\n"
		string += "NeedResponse=1\n"
		
		file.write(string)
		print("Config commands were written to ", path)
		file.close()

		Popen(["sudo", "usb_modeswitch", "-v", self.vendor_id, "-p", self.storage_product_id, "-c", path])
