#!/bin/bash

INTERNET_INTERFACE="ppp0"
WIFI_INTERFACE="wlan0"
AP_NAME="My Hotspot Wi-Fi"
AP_PASSWORD="12345678"

PATH=/bin:/usr/sbin:/usr/local/sbin:/sbin/:/usr/local/bin:/usr/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl /bin/python /bin/modem_manager.py --internet_interface=$INTERNET_INTERFACE --wifi_interface=$WIFI_INTERFACE --hotspot_ap_name="$AP_NAME" --hotspot_password="$AP_PASSWORD" wifi_try_start >> /tmp/log 2>&1 &
