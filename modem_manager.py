#!/bin/python
# LEC 2016, Router Anywhere
# Pîrvu Mihai Cristian, Mitrea Valentin Gabriel
from argparse import ArgumentParser
from wifi import Wifi
from time import sleep
from distutils.spawn import find_executable

def getArgs():
	parser = ArgumentParser()
	actions = ["start", "stop", "check", "switch", "wifi_start", "wifi_stop", "wifi_check", "wifi_try_start",
		"persistent_switch", "persistent_connect", "persistent_wifi"]
	parser.add_argument("action", metavar="action", type=str, help="Specify an action. Supported: " + str(actions))
	# Modem arguments
	parser.add_argument("--modem_type", type=str, default="modem", help="Manufacturer of the modem. Example: Huawei")
	parser.add_argument("--modem_model", type=str, default="Modem", help="Model of the modem. Example (for Huawei): "
		"E3131")
	parser.add_argument("--username", type=str, help="Username for modem")
	parser.add_argument("--password", type=str, help="Password for modem")
	parser.add_argument("--pin", type=str, help="PIN for SIM (optional)")
	parser.add_argument("--apn", type=str, help="Access Point Name for modem")
	parser.add_argument("--phone_number", type=str, help="Phone number for modem")
	parser.add_argument("--dev", type=str, help="USB slot where modem is connected")
	# Wi-Fi arguments
	parser.add_argument("--internet_interface", type=str, help="The name of the interface from which clients will " 
		"connect to via hotspot Wi-Fi", default=None)
	parser.add_argument("--wifi_interface", type=str, help="The name of the Wi-Fi interface that provices hotspot", 
		default=None)
	parser.add_argument("--hotspot_ap_name", type=str, help="Access Point Name for hotspot Wi-Fi", 
		default="Hotspot Wi-Fi")
	parser.add_argument("--hotspot_password", type=str, help="Password for hotspot Wi-Fi", default=None)
	# Persistent action (save or delete)
	parser.add_argument("--persistent_action", type=str, help="Persistent options: save or delete", default=None)

	args = parser.parse_args()
	type_and_model_required = False
	wifi_start = False
	if args.action in ["start", "persistent_connect", "persistent_switch"]:
		type_and_model_required = True
	if args.action == "wifi_start" or args.action == "wifi_try_start":
		wifi_start = True

	assert args.action in actions, "Action should be one of: " + str(actions)
	# assert so we can just call the script with stop/wifi_xxx and no modem type or modem model parameters
	assert type_and_model_required == False or (type_and_model_required == True and args.modem_type != "modem" and \
		args.modem_model != "Modem"), "Modem type and Modem model required" 
	assert args.modem_type in ["modem", "Huawei", "ZTE"]
	assert args.modem_model in ["Modem", "E3131", "MF190"]
	assert args.action != "wifi_start" or (args.action == "wifi_start" and args.internet_interface != None and \
		args.wifi_interface != None), "Source interface and wi-fi interface required"
	assert args.action[:10] != "persistent" or (args.action[:10] == "persistent" and args.persistent_action in \
		["save", "delete"]), "persistent_action should be save or delete"
	assert wifi_start == False or (wifi_start == True and args.internet_interface != None and \
		args.wifi_interface != None), "Source interface and wi-fi interface required"

	return args

def main():
	executables = ["modem_manager.py", "connect.sh", "wifi.sh"]
	for executable in executables:
		assert find_executable(executable), "Please create a symbolink link to" + executable + "in your PATH."

	args = getArgs()
	modem = None
	wifi = Wifi(args)

	if args.action[:4] != "wifi":
		# Dynamically import the class based on the modem type and modem model arguments
		module = __import__('modems.' + args.modem_type, fromlist=[args.modem_model])
		modem = getattr(module, args.modem_model)(args)

	# Modem connect actions 
	if args.action == "start":
		if modem.isRunning():
			modem.stop()
		modem.start()
	elif args.action == "stop":
		modem.stop()
	elif args.action == "check":
		if modem.isRunning():
			print("Modem is running.")
		else:
			print("Modem is not running.")
	elif args.action == "switch":
		modem.switch()
		print("Device should now be in modem mode.")
	# Hotspot Wifi actions
	elif args.action == "wifi_start":
		wifi.start()
	elif args.action == "wifi_try_start":
		while not wifi.isInternetInterface():
			print("Internet interface not found! Trying again in 10 seconds.")
			sleep(10)
		wifi.start()
	elif args.action == "wifi_stop":
		wifi.stop()
	elif args.action == "wifi_check":
		if wifi.isRunning():
			print("Wifi hotspot is up.")
		else:
			print("Wifi hotspot is down.")
	# Persistent actions
	elif args.action == "persistent_switch":
		modem.persistent_switch(persistent_action=args.persistent_action)
	elif args.action == "persistent_connect":
		modem.persistent_connect(persistent_action=args.persistent_action)
	elif args.action == "persistent_wifi":
		modem.persistent_wifi(persistent_action=args.persistent_action)

if __name__ == "__main__":
	main()
