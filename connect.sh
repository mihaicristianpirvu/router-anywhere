#!/bin/bash

MODEM_TYPE="Huawei"
MODEM_MODEL="E3131"
USERNAME="blank"
PASSWORD="blank"
APN="internet"
PHONE_NUMBER="*99#"
DEV="/dev/ttyUSB0"

PATH=/bin:/usr/sbin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl /bin/python /bin/modem_manager.py --modem_type=$MODEM_TYPE --modem_model=$MODEM_MODEL switch > /tmp/log 2>&1 &
PATH=/bin:/usr/sbin:/sbin/:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl /bin/python /bin/modem_manager.py --modem_type=$MODEM_TYPE --modem_model=$MODEM_MODEL --username=$USERNAME --password=$PASSWORD --apn=$APN --phone_number=$PHONE_NUMBER --dev=$DEV start >> /tmp/log 2>&1 &
