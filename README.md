# Router Anywhere
The project is written using **python3**, so please make sure the **python** command points to python3
```sh
$ sudo ln -s /usr/bin/python3.4 /usr/bin/python
```

You should also edit the **connect.sh**, **wifi.sh** and **switch.sh** files according to your system.
Finally, you should call
```sh 
$ make install
```
when you first clone the repository.

### Milestone 1
We had to connect to the internet using the usb modem (Digi was used for this test) from the Udoo Neo board. Using a scratch Udoobuntu for Udoo Neo, we had to install the following packages, so that our dialup script will work: **libusb**, **usb-modeswitch** and **wvdial**:
```sh
$ sudo apt-get install libusb-dev
$ sudo apt-get install usb-modeswitch
$ sudo apt-get install wvdial
```

After that, we had to configure our usb modem to be in Modem mode (instead of storage mode), so we used usb-modeswitch and the modem's configuration:
```sh
$ usb_modeswitch -v 12d1 -p 14fe -M '55534243123456780000000000000011062000000100000000000000000000' 
$ echo "12d1 1506" > /sys/bus/usb-serial/drivers/option1/new_id
```
Alternatively, to make it persistent, write to  /etc/udev/rules.d/70-huawei_e352.rules:
```sh
ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="12d1", ATTRS{idProduct}=="14fe", RUN+="/usr/sbin/usb_modeswitch -v 12d1 -p 14fe -M '55534243123456780000000000000011062000000100000000000000000000'"
ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="12d1", ATTRS{idProduct}=="14fe", RUN+="/bin/bash -c 'modprobe option && echo 12d1 1506 > /sys/bus/usb-serial/drivers/option1/new_id'"
```

Followed by:
```sh
udevadm control --reload-rules
```

or reboot

We plan to make the script more generic to automatically add these configs for every supported modem (which will be manually added by us).

After this, our modem is ready to be used, so we can call the dialup script, like this:
```sh
$ sudo python modem_manager.py Start
```

If the modem was set up to a different dev other than /dev/ttyUSB0 use:
```sh
$ sudo python modem_manager.py --dev=<path/to/dev/> Start 
```

To close the connection, use:
```sh
$ sudo python modem_manager.py Stop 
```

To check if the modem is connected, use: 
```sh
$ sudo python modem_manager.py Check 
```

Video for Milestone 1 can be seen here: https://www.youtube.com/watch?v=iAHfH14jVOs


### Milestone 2
For the second milestone, we had to turn the UDOO Neo board into a router, by making it provide hotspot wifi. In order to do this, we used the following script that helps us in creating a hotspot wifi network: https://github.com/oblique/create_ap. To install the script, simply clone the project and run the Makefile:
```sh
$ git clone https://github.com/oblique/create_ap.git
$ cd create_ap
$ make install
``` 
The following programs must also be installed: **dnsmasq**, **iptables**, **hostapd** and **haveged**:
```sh
$ sudo apt-get install dnsmasq
$ sudo apt-get install iptables
$ sudo apt-get install haveged
$ sudo apt-get install hostapd
```
Since most of the hard work is done by the script, we just have to run the correct calls to it and check if it's running. We've integrated the following 3 commands for our modem manager script regarding hotspot Wi-Fi:

```Start Wi-Fi```: Creates a hotspot interface using most of the create_ap default parameters. Required information is: the wi-fi interface which provides hotspot capabilities (eg: wlan0), the interface that provides internet (eg: wlan0(as well), eth0, ppp0 etc.), access point name and access point password for the hotspot interface.
```sh
$ sudo python modem_manager.py wifi_start --internet_interface=eth0 --wifi_interface=wlan0 --hotspot_ap_name="My Hotspot network" --hotspot_password="<password>"
```
```Stop Wi-Fi```: Stops the wi-fi process, removes the hotspot interface and disconnects all clients.
```sh
$ sudo python modem_manager.py wifi_stop
```
``` Check Wi-Fi```: Checks the status of the Wi-Fi process
```sh
$ sudo python modem_manager.py wifi_check
```

Video for Milestone 2 can be seen here: https://www.youtube.com/watch?v=8FOP9sCAH68

### Milestone 3 and Bonus
For the final milestone, we had to make the system autonomous. That means, if a device is detected, it should automatically try to switch to mode mode, connect to the internet and create an Wi-Fi hotspot interface. The scripts that are run during this detection are **switch.sh**, **connect.sh** and **wifi.sh**. To achieve this, we used Linux's udev rules. One problem with this is that when the scripts are run, the enviroment is empty, and there is no PATH variable set. Because some internal python programs require PATH variable to be set, it is important that all the scripts are run with a temporary PATH variable, like this:

```sh
PATH=<custom_path> /bin/python /bin/modem_manager.py --internet_interface=$INTERNET_INTERFACE --wifi_interface=$WIFI_INTERFACE --hotspot_ap_name="$AP_NAME" --hotspot_password="$AP_PASSWORD" wifi_try_start >> /tmp/log 2>&1 &
```

Because of this reason, all those scripts must also be placed in /bin/ folder or create a symlink to the original ones. The makefile we provide does exactly this. So in order for things to work smoothly, we recommend using 
```sh
make install
```
before running anything on a new system, as well as updating the 3 .sh files accordingly to your system and your used 3G modem.

The udev rules are set based on information provided to modem_manager.py script. 3 new actions have been added + an aditional action for saving or deleting an existing configuration. The 3 optionss are ```persistent_switch```, ```persistent_wifi``` and ```persistent_connect```. The optional action only matters when one of these 3 actions are used: ```--persistent_action=save``` or ```--persistent_action=delete```

For example, to add persistent switching, when an unswitched modem is detected, the command is this:
```sh
sudo python modem_manager.py --modem_type=Huawei --modem_model=E3131 --persistent_action=save persistent_switch
```
Video for Milestone 3 and Bonus can be seen here: https://www.youtube.com/watch?v=HC2txyiGM7o